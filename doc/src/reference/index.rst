.. _reference:

Reference
=========

SpeedCrunch includes a large number of built-in math functions and constants. They
are documented in this reference, organized by categories. For an alphabetical list,
consult the :ref:`genindex` and the :ref:`sc:functionindex`.


Built-in Functions
------------------


.. toctree::
   :maxdepth: 2

   basic
   integer
   statistical
   ieee754


Constants
---------

.. toctree::
   :maxdepth: 2

   constants


.. TODO: What else needs to be documented? Units, I guess?
